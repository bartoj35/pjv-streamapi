import java.time.LocalDate;
import java.time.Year;

public class Person
{
    private String name;
    private String surname;
    private LocalDate birthDate;
    private int salary;

    public Person ( String name, String surname, LocalDate birthDate, int salary )
    {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.salary = salary;
    }

    public String getName ( )
    {
        return name;
    }
    public String getSurname ( )
    {
        return surname;
    }
    public LocalDate getBirthDate ( )
    {
        return birthDate;
    }
    public int getSalary ( )
    {
        return salary;
    }

    public int getAge ( )
    {
        return LocalDate.now () . getYear ( ) - birthDate . getYear ();
    }

    public int surnameLen ( )
    {
        return surname . length ();
    }


    @Override
    public String toString ( )
    {
        return "'surname='" + surname + "\' " + ", name='" + name + "\' " + ", age=" + getAge () + ", salary=" + salary;
    }
}
