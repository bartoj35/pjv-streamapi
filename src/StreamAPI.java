import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class StreamAPI
{
/*
    Vypište všechny osoby, které jsou starší 20 let, mají příjmení delší než 8 znaků a ve jméně mají písmeno M.
    Vypište je seřazené podle abecedy, nejprve příjmení, pak jméno a následně dle data narození od nejstaršího po nejmladšího.
    Zjistěte, zda a případně kolikrát je obsažen Jan Novák.
    Vypište pět příjmení, která nejvíce vydělávají.

 */
    public static void main ( String[] args )
    {
        List < Person > people = GeneratePerson . generate ( 1000000 );
        Predicate < Person > surnameLen = person -> person . surnameLen () > 8;
        Predicate < Person > containsM = person -> person . getName () . contains ( "M" );
        Predicate < Person > isJanNovak = person -> person . getName () . equalsIgnoreCase ( "Jan" ) &&
                                        person . getSurname (). equals ( "Novák" );
        //people . stream () . forEach ( person -> System.out.println ( person . toString ( ) ) );
        /*people . stream () . filter ( person -> person . getAge () > 20 )
                           . filter ( surnameLen )
                           . filter ( containsM )
                            . sorted ( Comparator . comparing ( Person :: getSurname )
                                        . thenComparing ( Person::getName )
                                        . thenComparing ( Person::getBirthDate, LocalDate::compareTo ) )
                            . forEach ( System.out::println );*/
                            //. forEach ( person -> System.out.println ( person . toString () ) );
        long count = people . stream () . filter ( isJanNovak )
                            . count (  ) ;
        System.out.println ( "Janu Novaku je " + count );
        people . stream () . sorted ( Comparator . comparing ( Person::getSalary ) . reversed () )
                           . limit ( 5 )
                           . forEach ( System . out :: println );
    }
}
