import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class GeneratePerson
{
    private static Random randomGenerator = new Random ( 42 );

    private GeneratePerson ( ) {  }

    public static List < String > readFromFile ( String path )
    {
        Path file = new File ( path ).toPath(); //otevře soubor
        List <String> result = null;
        try
        {
            result = Files.lines(file) // Stream, kde každá položka reprezentuje jeden řádek vstupu
                    .collect( Collectors.toList()); //vytvoření kolekce
        }
        catch ( IOException e )
        {
            e . printStackTrace ();
        }
        return result;
    }

    private static String randomFromList ( List < String > list )
    {
        return list . get ( randomGenerator . nextInt ( list . size () ) );
    }

    private static LocalDate randomDate ( )
    {
        int minDay = (int) LocalDate.of(1900, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2015, 1, 1).toEpochDay();
        long randomDay = minDay + randomGenerator .nextInt(maxDay - minDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    private static int randomSalary ( )
    {
        int minSalary = 15200;
        int maxSalary = 300000;
        return randomGenerator . nextInt ( maxSalary - minSalary ) + minSalary;
    }

    public static Person generatePerson ( List < String > name, List < String > surname )
    {
        return new Person ( randomFromList ( name ), randomFromList ( surname ), randomDate (), randomSalary () );
    }


     public static List < Person > generate ( int count )
     {
         if ( count < 0 ) { throw new IllegalArgumentException ( "Not valid count!" ); }

         List < String > namesFemale = readFromFile ( "jmena/100nejDivky.txt" );
         List < String > namesMale = readFromFile ( "jmena/100nejChlapci.txt" );
         List < String > surnamesMale = readFromFile ( "jmena/PrijmeniMuzi.txt" );
         List < String > surnamesFemale = readFromFile ( "jmena/PrijmeniZeny.txt" );

         List < Person > result = new ArrayList <> ( count );
         if ( count % 2 != 0 )
         {
             result . add ( generatePerson ( namesMale, surnamesMale ) );
         }

         for ( int i = 0; i < count / 2; i++ )
         {
                 result . add ( generatePerson ( namesFemale, surnamesFemale ) );
                 result . add ( generatePerson ( namesMale, surnamesMale ) );
         }
         return result;
    }
}
